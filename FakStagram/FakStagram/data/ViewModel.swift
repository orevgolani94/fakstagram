//
//  ProfileViewModel.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 21/02/2021.
//

import Foundation


struct ViewModel {
    
    private var selectedProfile : [User]!
    var users : [User]
    
    init(users : [User]) {
        self.users =  users
    }
    
    mutating func getSelectedProfile(from userId : Int) -> [User]{
        selectedProfile = self.users.filter{$0.user_id == userId}
        return selectedProfile
    }

    
    func getCollectionOfImageUrlFromProfile() -> [String]{
        guard let selectedProfile = selectedProfile else {
            print("selected profile wasnt initialized")
            return []}
        
        return selectedProfile.map{$0.image.webformatURL}
    }
    
    
}
