//
//  DataManager.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import Foundation
import Combine

class DataManager : ObservableObject {
    
    static var error : String?
    static var print : String?
    
    var viewModel : ViewModel!
    var api : Request?
    var count =   0
    
     var data : [User] = []
     var selectedProfile : [User] = []
     var picturesForSelectedProfile : [ImageDl] = []
     var collectionOfImageForProfile : [String] = []

    
    init() {
        api = Request(self)
        getData()
    }
    
    
    func setupViewModel(){
        viewModel =  ViewModel(users: data)
    }
    
    func getData(){
        api?.getUsers{}
    }
    
    func selectProfile(with user_id : Int, completion : @escaping(()->Void)){
        self.selectedProfile = viewModel.getSelectedProfile(from: user_id)
        completion()
    }
    

    
     func getCollectionOfImage(){
        collectionOfImageForProfile =  viewModel.getCollectionOfImageUrlFromProfile()
    }
    
    
    func getProfile(from user_id : Int){
        
       let oneUser =   data.filter{$0.user_id == user_id}
        oneUser.forEach{$0.toString()}
    }
    
    
}


extension DataManager : RequestDelegate {
    func downloaded(users: [User]) {
        
        DispatchQueue.main.async {
            self.data = users
            self.setupViewModel()

        }
        

    }
    
    
}
