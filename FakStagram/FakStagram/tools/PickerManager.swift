//
//  PickerManager.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 23/02/2021.
//

import Foundation
import SwiftUI

struct PickerManager : UIViewControllerRepresentable {
    
    @Environment(\.presentationMode) var presentationMode
    @Binding var image : UIImage?
    
    class Coordinator : NSObject , UINavigationControllerDelegate, UIImagePickerControllerDelegate{
        let parent : PickerManager
        
        init(_ parent : PickerManager){
            self.parent = parent
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let uiImage = info[.originalImage] as? UIImage{
                parent.image = uiImage
            }
            parent.presentationMode.wrappedValue.dismiss()
        }
    }
    
    func makeUIViewController(context: Context) -> some UIViewController {
        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.delegate =  context.coordinator
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

}

