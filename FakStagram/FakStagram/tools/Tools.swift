//
//  Tools.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 23/02/2021.
//

import Foundation
import UIKit

struct Tools {
    
    static func hideKeyboard(){
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
