//
//  User.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import Foundation

struct UserProfile{
    var user_id  : Int
    var user : String
    var userImageURL : String
    var imagesLinks : [ImageDl]

}

struct Hits : Codable {
    
    var hits : [User]
    
    func toString() {
        hits.forEach{ user in
            user.toString()
        }
    }
}
struct User : Codable, Identifiable {
    
    var user_id  : Int
    var user : String
    var userImageURL : String
    var tags : String
    
   private var webformatURL : String
   private var likes : Int
   private var comments :Int
    
    var id : Int {
        return  user_id
    }

    
    var image : ImageDl {
        return ImageDl(user_id: user_id, webformatURL: webformatURL, likes: likes, comments: comments)
    }
    
    
    func toString() {
        print("user_id : \(user_id)  \n user : \(user)  \n userinageurl \(userImageURL)")
    }

}


struct ImageDl : Codable {
    var user_id : Int
    var webformatURL : String
    var likes : Int
    var comments :Int
}
