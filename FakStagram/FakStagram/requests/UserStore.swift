//
//  UserStore.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 17/02/2021.
//

import Foundation

protocol UserStore {
    var users : [User]{get}
}
