//
//  Request.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 17/02/2021.
//

import Foundation

protocol RequestDelegate {
    
    func downloaded(users : [User])
    
}

struct Request {
    let urlString  = "https://pixabay.com/api/?key=16392371-6077ea3485725ef639f40b646&image_type=all&pretty=true&per_page=200"
    var delegate : RequestDelegate

    
    init(_ delegate : RequestDelegate) {
        self.delegate = delegate
    }
    

    func getUsers( finish : @escaping ((())->Void) )  {
        var users : [User] = []

        let request = URLRequest(url: URL(string: urlString)!)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            DataManager.error = error?.localizedDescription
            if let error = error {
                print(error.localizedDescription)
            }


            if let data = data {
                do {
                    let hits = try JSONDecoder().decode(Hits.self, from: data)
                    users = hits.hits
                    finish(delegate.downloaded(users: users))
                }catch let error{
                    print("Request => getUsers => error \(error)")
                }
                

            }
        }.resume()
        
    }
    


    
}
