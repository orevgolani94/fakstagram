//
//  FakStagramApp.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import SwiftUI

@main
struct FakStagramApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationScreen()
        }
    }
}
