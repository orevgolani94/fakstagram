//
//  HeaderProfile.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 22/02/2021.
//

import SwiftUI

struct HeaderProfile: View {
    
    var profileImageLink : String
    var userName : String = ""

    var body: some View {
        VStack {
            
            HStack(alignment: .top) {
                VStack(alignment : .leading) {
                    Text(userName )
                        .font(.title3)
                        .fontWeight(.bold)
                    HStack(alignment: .top){
                        Text("14456 followers")
                            .font(.caption2)
                        Text("398 following")
                            .font(.caption2)
                    }
                }
                .padding(.horizontal)
                Spacer()
                StoryIcon(profileImageURL: profileImageLink, username: "", width: 80)
                    .padding(.horizontal)

            }
            
            HStack{
                HStack{
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Text("Follow")
                            .foregroundColor(.white)
                    })
                    .frame(width: 180, height: 35, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .background(Color.blue)
                    .cornerRadius(4.0, antialiased: true)
                    
                    Spacer()
                    
                    Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                        Text("Message")
                            .foregroundColor(.black)
                    })
                    .frame(width: 180, height: 35, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .background(Color.white)
                    .cornerRadius(4.0, antialiased: true)
                    .overlay(RoundedRectangle(cornerRadius: 4.0)
                                .stroke(lineWidth: 0.5).foregroundColor(.gray)
                    )


                }
                .padding(.horizontal)
            }
            
        }

        
        
    }
}

struct HeaderProfile_Previews: PreviewProvider {
    static var previews: some View {
        HeaderProfile(profileImageLink: "https://cdn.pixabay.com/user/2020/11/30/11-19-23-84_250x250.jpg", userName: "Jhonny")
    }
}
