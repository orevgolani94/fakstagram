//
//  CustomInage.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 22/02/2021.
//

import SwiftUI
import URLImage

struct CustomImage: View {
    
    var  link : String
    
    @ViewBuilder
    var body: some View {
        if let  url = URL(string: link){
            
            URLImage(url: url,
                     content: { image in
                         image
                            .resizable()
                            .scaledToFit()

                     })
        
                }else{
                    Image(systemName: "person")
                        .resizable()
                        .scaledToFit()

        
                }
        
    }
}

struct CustomImage_Previews: PreviewProvider {
    static var previews: some View {
        CustomImage(link: "")
    }
}
