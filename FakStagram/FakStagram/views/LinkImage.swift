//
//  LinkImage.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 22/02/2021.
//

import SwiftUI

struct LinkImage: View {
    
    var link : String
    
    var body: some View {
   
        ZStack{
            NavigationLink(
                destination : Text(link)){
                 EmptyView()
            }.hidden()
            CustomImage(link : link)
            
        }
        
    }
}

struct LinkImage_Previews: PreviewProvider {
    static var previews: some View {
        LinkImage(link: "")
    }
}
