//
//  CollectionPhotoList.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 22/02/2021.
//

import SwiftUI
import URLImage

protocol CollectionDelegate {
    
}

struct CollectionPhotoList: View{
    @ObservedObject var  manager : DataManager
    var list : [String]
    var arrOfIndexes : [Int] = []
    
    func range() -> Int{
        return list.count / 3 > 0 ? list.count / 3 : 1
    }
    
    func imageReturned(line : Int, col :Int) -> LinkImage?{
        guard  list.count > 0 else {
            return nil
        }
        if line > 0 {
            let index =  col + 3
            if list.count > index {
                return LinkImage(link: list[index])

            }
        }else{
            if list.count > col {
                return LinkImage(link: list[col])

            }

        }
        return nil
    }

    var body: some View {
        List() {
            ForEach(0..<range()) { line  in
                    HStack {
                        ForEach(0..<3) { col   in
                                imageReturned(line: line, col: col)
                                    .shadow(radius: 4)
                            
                        }
                        .lineSpacing(1)
                        
                 }
            }
            
        }
        
    }
}

struct CollectionPhotoList_Previews: PreviewProvider {
    static var previews: some View {
        CollectionPhotoList(manager: DataManager() ,list: ["https://pixabay.com/get/gac71a8295b0193d686abfd740b047eba4b3c4de3ef479699516cf8bb7f4dbe3791a3411f8eac0cb3470616aeae7545c5d3aeb6e0ca3ebbdba6f7b60ef4a0c25d_640.jpg",
                                   "https://pixabay.com/get/g9fa59a3d62f36577442afb1f01163e643cce755fd7312cf992178512eb074971138e20dc5b732c121e0f89407babc31d522e7aaa51d9bd70b55b8631819c665c_640.jpg",
                                   "https://pixabay.com/get/g90e1d939f1b929e214512bc90d811c72b5610097179ae2f28d1b7ffa1087c4e43d02277379fc723dc26d4286b8b6506d2e1f2a5a878ac62e26e76f8b953f4c0d_640.jpg"
        ])
    }
}
