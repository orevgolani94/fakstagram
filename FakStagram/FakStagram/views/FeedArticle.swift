//
//  FeedArticle.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import SwiftUI
import URLImage

struct FeedArticle: View {
    
    var profileImageURL : String
    var username : String
    var tags : String
    var imageURL : String
    var likes : Int
    var comments : Int
    
   var heartImage =  Image(systemName: "suit.heart")
    @State var clicked : Bool =  false
    @State var likeUp : Int = 0

    
    @ViewBuilder
    var body: some View {
        
        VStack{
            
            HStack(alignment: .center) {
                
                if let  url = URL(string: profileImageURL){
                    URLImage(url: url,
                             content: { image in
                                 image
                                     .resizable()
                                     .aspectRatio(contentMode: .fit)
                             })
                        .frame(width: 40, height: 40, alignment: .leading)
                        .cornerRadius(20)
                        .overlay(
                            Circle()
                                .stroke(lineWidth: 0.1)
                        )
                        .shadow(radius: 8)

                }else{
                    Image(systemName: "person")
                        .frame(width: 40, height: 40, alignment: .leading)
                        .cornerRadius(20)
                        .overlay(
                            Circle()
                                .stroke(lineWidth: 0.1)
                        )
                        .shadow(radius: 8)

                    
                }
                    

               
                
                VStack(alignment: .leading){
                    Text(username)
                        .font(.caption)
                        .fontWeight(.semibold)
                        .multilineTextAlignment(.leading)
                        
                    Text("Irish forest of Sherburg")
                        .font(.caption2)
                }
                
                Spacer()
                Button(action: {
                    
                }, label: {
                    Image(systemName: "ellipsis")
                        .foregroundColor(.black)
                })
            }
            
            .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
            
            
            if let url = URL(string: imageURL) {
                URLImage(url: url,
                         content: { image in
                             image
                                 .resizable()
                         })
                    .frame(width: 300, height: 200, alignment: .center)
                    .overlay(
                        Rectangle()
                            .stroke(lineWidth: 2)
                            .foregroundColor(.orange)
                    )
                    .accentColor(.clear)
                    .shadow(color: .black, radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/, x: 3, y: 2)

                
            }else {
                Image("dog")
                    .resizable()
                    .frame(width: 300, height: 200, alignment: .center)
                    .overlay(
                        Rectangle()
                            .stroke(lineWidth: 2)
                            .foregroundColor(.orange)
                    )
                    .accentColor(.clear)
                    .shadow(color: .orange, radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/, x: 3, y: 2)

            }
   
            
            HStack(alignment:.center) {
                VStack {
                    Button(action: {
                        clicked.toggle()
                        if clicked {
                            likeUp += 1
                        }else{
                            likeUp -= 1
                        }
                    }
                    , label: {
                        VStack {
                            Image(systemName: clicked ? "suit.heart.fill" : "suit.heart")
                                
                                .foregroundColor(clicked ? .red : .black)
                          
                        }
                            
                })
                    
                    
                }
              
            
                
                Button(action: {
                    
                }
                , label: {
                        Image(systemName: "bubble.right")
                            .foregroundColor(.black)
                })
              
                
                Button(action: {
                        
                }
                , label: {
                    Image(systemName: "paperplane")

                            .foregroundColor(  .black)
                })
                

                
                Spacer()
               

            }
            .padding(EdgeInsets(top: 5, leading: 20, bottom: 0, trailing: 0))
            HStack{
                Text("liked by \(likeUp)")
                Spacer()
            }
            .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 0))


        }
        .padding()
        .onAppear(perform: {
            likeUp =  likes
        })
    }
}

struct FeedArticle_Previews: PreviewProvider {
    static var previews: some View {
        FeedArticle(profileImageURL: "https://cdn.pixabay.com/user/2016/07/26/16-51-09-475_250x250.jpg",
                    username: "Jack", tags: "On the road", imageURL: "https://pixabay.com/get/gbc86838dade6dc347936a0e9894a03c60560b42ef9b007a8e41929900852b210fa045e12b3b8689d54fd093af6390a49677f8bb979324fb92ddbe8fea1d8134b_640.jpg",
                        likes: 8,
                        comments: 6
                    )
    }
}
