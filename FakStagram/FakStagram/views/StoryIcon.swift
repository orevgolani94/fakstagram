//
//  storyIcon.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import SwiftUI
import URLImage

struct StoryIcon: View {
    
    var profileImageURL : String
    var username : String
    var width : CGFloat
 
    
    
    
    
    @ViewBuilder
    var body: some View {
        VStack{
            if let  url = URL(string: profileImageURL){

                URLImage(url: url,
                         content: { image in
                             image
                                 .resizable()
                         })
                    .frame(width: width, height: width, alignment: .leading)
                    .cornerRadius(width / 2)
                    .overlay(
                        Circle()
                            .stroke(lineWidth: 0.1)
                )

            }else{
                Image(systemName: "person")
                    .resizable()
                    .frame(width: width, height: width, alignment: .leading)
                    .cornerRadius(width / 2)
                    .overlay(
                        Circle()
                            .stroke(lineWidth: 0.1)
                )
                
            }
            
            
            Text(username)
                .font(.caption2)
                .fontWeight(.medium)
                .multilineTextAlignment(.center)
                
        }
        
    }
}

struct storyIcon_Previews: PreviewProvider {
    static var previews: some View {
        StoryIcon(profileImageURL: "https://cdn.pixabay.com/user/2020/01/12/12-31-35-440_250x250.png", username: "Pikachu", width: 40)
    }
}
