//
//  StoryList.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import SwiftUI

struct StoryList: View {
    
    @EnvironmentObject var manager : DataManager
    var showButtonUpload : Bool
    
    var body: some View {
        VStack(alignment: .leading, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/){
            ScrollView(.horizontal, showsIndicators: false ){
                HStack(alignment : .center) {
                    ForEach(manager.data , id :\.id) { user in
                        StoryIcon(profileImageURL: user.userImageURL , username : user.user, width: 40.0)
                        }
                    }
                
            }
            .shadow(radius: 8)

            if self.showButtonUpload {
                Button(action: {}
                       , label: {
                        HStack{
                        Image(systemName: "plus.circle.fill")
                            .foregroundColor(.orange)
                            Text("Upload new story")
                                .foregroundColor(.gray)
                        }
                })
                    .frame(alignment : .leading)
            }
               
        }
        .frame(height: 110)
        
        
    }
}

struct StoryList_Previews: PreviewProvider {
    static var previews: some View {
        StoryList( showButtonUpload: true)
            .environmentObject(DataManager())
            
    }
}
