//
//  NewPostScreen.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import SwiftUI

struct NewPostScreen: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct NewPostScreen_Previews: PreviewProvider {
    static var previews: some View {
        NewPostScreen()
    }
}
