//
//  UserScreen.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 17/02/2021.
//

import SwiftUI

struct UserScreen: View {
    
    @ObservedObject var manager  : DataManager
    @State var profileImageLink : String = ""
    @State var userName : String = ""
    @State var images : [String] = []
    var id : Int

     var body: some View {

        VStack {
            
            HeaderProfile(profileImageLink: profileImageLink , userName: userName)
            StoryList(showButtonUpload: false)
            CollectionPhotoList(manager: manager, list: images)

        }
        
        //AT THE END
        .onAppear(perform: {
                manager.selectProfile(with: id){
                    manager.getCollectionOfImage()
                    profileImageLink = manager.selectedProfile[0].userImageURL
                    userName = manager.selectedProfile[0].user
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        let temp = manager.collectionOfImageForProfile
                        images = temp.removeDuplicates()
                        images.forEach{print($0)}


                    }
                }})
    }
}

struct UserScreen_Previews: PreviewProvider {
    static var previews: some View {
        UserScreen(manager: DataManager(), id: 1777190)
        
    }
}


extension Array where Element: Hashable {
    
    func removeDuplicates() -> [Element]{
        var result = [Element]()
        for value in self {
            if !result.contains(value){
                result.append(value)
            }
        }
        return result
    }

    func uniqued() -> [Element] {
            var seen = Set<Element>()
            return filter{ seen.insert($0).inserted }
        }
    
    func unique<T: Hashable>(for keyPath: KeyPath<Element, T>) -> [Element] {
        var unique = Set<T>()
        return filter { unique.insert($0[keyPath: keyPath]).inserted }
    }
    
}
