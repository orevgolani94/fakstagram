//
//  FeedScreen.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import SwiftUI

struct FeedScreen: View {
    var index : Int = 0
    @EnvironmentObject var manager : DataManager
    

    
    var body: some View {
        VStack{
            StoryList(showButtonUpload: true )
            Spacer()
            ScrollView(.vertical){

                ForEach(manager.data, id : \.id){ item in
                    FeedArticle(profileImageURL: item.userImageURL,
                                username : item.user,
                                tags : item.tags,
                                imageURL : item.image.webformatURL,
                                likes: item.image.likes,
                                comments: item.image.comments
                    )

                }
            }
            
            
 
        }
        .onAppear{
            manager.data.forEach{$0.toString()}
        }
       

    }
}

struct FeedScreen_Previews: PreviewProvider {
    static var previews: some View {
        FeedScreen()
            .environmentObject(DataManager())
    }
}
