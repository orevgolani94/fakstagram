//
//  SearchScreen.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import SwiftUI

struct SearchScreen: View  {
    @ObservedObject var manager : DataManager
    @State private var searchText : String = ""

    
    func getNameList() ->[String]{
        let arr =  manager.data.map{$0.user}
        let result = arr.removeDuplicates()
        return result
    }
    
    func getUserId(username : String) -> Int {
        for user in manager.data{
            if user.user == username {
                return user.user_id
            }
            
        }
        return 0
    }
    
    func getSearchText()-> String{
        if self.searchText.isEmpty {
                Tools.hideKeyboard()
        }
        return searchText
    }
    
    var body: some View {
        
        NavigationView{
            VStack {
                SearchBar(text: $searchText)

                List{
                    ForEach(getNameList().filter{ $0.contains(getSearchText())}, id:\.self){ item in
                        NavigationLink(
                            destination: UserScreen(manager: manager, id: getUserId(username: item)),
                            label: {
                                Text(item)
                            })
                    }
                }

            }
            .navigationTitle("Search User")

        }

        
    }
}

struct SearchScreen_Previews: PreviewProvider {
    static var previews: some View {
        SearchScreen(manager: DataManager())
         .environmentObject(DataManager())
    }
}
