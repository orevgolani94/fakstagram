//
//  ProfileScreen.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import SwiftUI

struct ProfileScreen: View {
    
    @State private var showPicker = false
    @State private var inputImage : UIImage?
    @State private var image : Image?
    @EnvironmentObject var manager : DataManager
    let imageList = [
        "https://pixabay.com/get/g93642b227f1c804bfc43a4f2a4792b1488dbe05126b85fbd8688ce95b6a31ffad0f8c63d32aa3d832c2424073c27c2a995d37b13a3b019de2668addf539a96ed_640.jpg",
        "https://pixabay.com/get/g5b79de85624f9df2d4b9f69bc8ccaf33b470f7281649b2dbcb9c6bb411114ba1096e58930ea6a3708a1a3aa6915295fd59c5aa99c37321dd782743a0a622aecc_1280.jpg",
        "https://pixabay.com/get/g8a97856b05d0b0b883d8ff4d7332b114bb5d36ffd830ab51d668e2b73f4d981a7294eb6790dd1073a6eb933971f39bcfae010f251dd66ae48cf4fb1ed2e28936_640.jpg",
        "https://pixabay.com/get/gc255a6d13cbf42c6c52c9f40daea2174477a7119404e67fa944db4d0297d1ef04d881adb6609641fbc051af161f1cd266ed28f66c550b675c1d6ae467963915e_1280.jpg",
        "https://pixabay.com/get/gb2e984fc37bb93b88632d75a47b94d41fd10437c10af7d8505b367fbca12774eba92267089a30d355ab17eda6ae60137_640.png",
        "https://pixabay.com/get/g76b255778bfa293ce1d2c40511a81c0bca75acffab432f3399a7f6b1438a59fb62c00c8f9dea96368d97bdd5ff815e3eca94c0c05e18d9d41fc72a4f0e14a261_1280.jpg"
        
    ]
    
    func loadImage(){
        guard let inputImage = inputImage  else {return}
        image = Image(uiImage: inputImage)
    }
    @ViewBuilder
    var body: some View {
        VStack{
            ZStack(alignment:.bottom) {
                if let image = image{
                    image
                        .resizable()
                        .frame(width : 120 , height : 120 , alignment: .center)
                        .aspectRatio(contentMode: .fit)
                        .cornerRadius(60)
                        .overlay(Circle().stroke(lineWidth: 0.5).foregroundColor(.gray))
                    
                }else{
                    StoryIcon(profileImageURL: "", username: "Ruben", width: 120)
                }
                
                HStack{
                    Spacer()
                    Button(action: {
                        print("clicked")
                        showPicker =  true
                    }, label: {
                        Image(systemName: "plus.circle.fill")
                            .resizable()
                            .frame(width: 40, height: 40, alignment: .center)
                            .foregroundColor(.red)
                            .background(Color.white)
                            .cornerRadius(20)
                    })
                }.offset(y: -10)
            }
            .frame(width: 120)
            Text("https://msapps.mobi/")
                .font(.caption)
                .foregroundColor(.gray)
            HStack(alignment: .center){
                Spacer()

                VStack{
                    Text("100")
                        .fontWeight(.bold)
                    Text("posts")
                        .font(.caption)
                        .foregroundColor(.gray)
                        
                }
                .frame(width: 55)
                Spacer()
                VStack{
                    Text("100")
                        .fontWeight(.bold)
                    Text("followers")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
                .frame(width: 55)


                Spacer()
                VStack{
                    Text("1k")
                        .fontWeight(.bold)
                    Text("following")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
                .frame(width: 55)
                Spacer()

            }
            .padding([.horizontal, .top])
            
            CollectionPhotoList(manager: manager , list: imageList)
        }
        .sheet(isPresented: $showPicker, onDismiss: loadImage, content: {
            PickerManager(image: self.$inputImage)
        })
    }
    
}

struct ProfileScreen_Previews: PreviewProvider {
    static var previews: some View {
        ProfileScreen()
            .environmentObject(DataManager())
    }
}
