//
//  TabView.swift
//  FakStagram
//
//  Created by Ruben Mimoun on 14/02/2021.
//

import SwiftUI

struct NavigationScreen: View {
    
     var manager = DataManager()
     @State var hasData  = false

    
    init(){
  
        UITabBar.appearance().backgroundColor = UIColor.orange.withAlphaComponent(0.4)
        
        print("Hello Im Paul")

    }
    
    
    
    var body: some View {
        
            TabView {
                ProfileScreen()
                    .tabItem {
                        Label("", systemImage : "person")
                            
                    }
                    
                
                FeedScreen()
                    .tabItem {
                        Label("", systemImage: "house")
                            .foregroundColor(.red)

                    }
                    
                
                SearchScreen(manager: manager)
                    .tabItem {
                        Label("", systemImage: "magnifyingglass")
                    }
                
                NewPostScreen()
                    .tabItem {
                        Label("", systemImage: "plus.app")
                    }
                
                LikesScreen()
                    .tabItem {
                        Label("", systemImage: "suit.heart")
                    }
                
  
               
            }
            .environmentObject(manager)
            .accentColor(.purple)
            .onAppear(perform: {
                if !hasData{
                    manager.getData()
                    hasData =  true
                }
                
            })
        
        
        
    }
}

struct BottomNavigationTabs_Previews: PreviewProvider {
    static var previews: some View {
        NavigationScreen()
    }
}
